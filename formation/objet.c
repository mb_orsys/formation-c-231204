#include <stdio.h>
#include <stdlib.h>

#include "objet.h"

void afficher_destination(Destination* d) {
    printf("%s : %d j\n", d->nom, d->jours);
}
void allonger_destination(Destination* d, short j) {
    if(j > -d->jours)
        d->jours += j;
}
void raccourcir_destination(Destination* d, short j) {
    d->allonger(d, -j);
}

Destination* destination_ctor(const char * nom, unsigned short jours) {
    Destination* self = malloc(sizeof(Destination));
    self->nom = nom;
    self->jours = jours;
    self->afficher = afficher_destination;
    self->allonger = allonger_destination;
    self->raccourcir = raccourcir_destination;
    return self;
}
void destination_dtor(Destination* self) {
    // NON, pas dans le tas : free(self->nom);
    free(self);
}

void objet() {
    printf("* Objet :\n");

    Destination* d1 = destination_ctor("Madrid", 3);
    d1->allonger(d1, 4);
    d1->raccourcir(d1, 2);
    d1->afficher(d1); // Madrid, 5j
    destination_dtor(d1);

    Destination* d2 = destination_ctor("Barcelone", 2);
    d2->afficher(d2);
    destination_dtor(d2);
}
