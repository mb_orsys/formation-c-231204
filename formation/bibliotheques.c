#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <signal.h>
#include <stdint.h>
#include <math.h>
#include <complex.h>
#include <stdbool.h>
#include <time.h>
#include <stdarg.h>


void memoire() {
    int lignes = 3;
    char conditions[1000];
    // NON, trop gros (10M) char conditions[10000000];
    printf("Emplacements pile : %p %p\n", &lignes, conditions); //0061FDBC 0061F9D0
    // #include <stdlib.h>
    void * petites_lignes = malloc(100*1000*1000); //100Mo
    if(petites_lignes == NULL)
        printf("Allocation echou�e\n");
    else
        printf("Emplacements tas : %p\n", petites_lignes); // 00AD5040
    free(petites_lignes);
    printf("Emplacements tas : %p\n", petites_lignes); // 00AD5040
    // INTERDIT ((char*)petites_lignes)[3] = 23;
    // INTERDIT free(petites_lignes);
    // INTERDIT free(conditions);
    // INTERDIT free(petites_lignes+8);
    petites_lignes = NULL;
    free(petites_lignes); // OK mais inutile

    void* zone_memoire = malloc(1200);
    char* chaine_validation = (char*)zone_memoire;
    *chaine_validation = '?';
    chaine_validation++;
    *chaine_validation = 0;
    chaine_validation--;
    printf("Validation : %s\n", chaine_validation);
    free(zone_memoire);
    // OU, pas ET : free(chaine_validation);

    char* chaine_confirmation = malloc(1200);
    // OU char* chaine_confirmation = (char *) malloc(1200);
    free(chaine_confirmation);
}


unsigned lire_villes_americaines1(char ** villes) {
    villes[0] = "New York";
    villes[1] = "Las Vegas";
    villes[2] = "La Nouvelle Orleans";
    return 3;
}

char ** lire_villes_americaines2() {
    char** villes = malloc(3 * sizeof(unsigned char*));
    villes[0] = "New York";
    villes[1] = "Las Vegas";
    villes[2] = "La Nouvelle Orleans";
    return villes;
}

void communication_memoire() {

    char * villes1[3]; // sans malloc
    lire_villes_americaines1(villes1);
    printf("Premiere ville : %s\n", villes1[0]);

    char ** villes2 = lire_villes_americaines2();
    printf("Premiere ville : %s\n", villes2[0]);
    free(villes2);
}

void memoire_allocations() {
    float* distances = calloc(3, sizeof(float)); // probablement 12 octets
    printf("Adresse tas calloc : %p\n", distances); // 00A56BB0
    distances[0] = 4345;
    printf("Distance 1 : %f\n", distances[1]); // 0.00000
    float* dist2 = realloc(distances, 6*sizeof(float)); // exactement 24o
    if(dist2 != distances)
        printf("Nouveau bloc\n");
    printf("Distance 0 : %f\n", dist2[0]); // 4345.0
    // NON printf("Distance 1 : %f\n", distances[1]);
    free(dist2);

     // MinGW, MSVC
     float* distances3 = _aligned_malloc(3*sizeof(float), 8);
     // GCC, Clang
     // float* distances3 = aligned_malloc(8, 3*sizeof(float));
     printf("Adresse tas aligne : %p\n", distances3); // 00A96BB8
     _aligned_free(distances3);
     // free(distances3); // GCC, Clang
}

// #include <string.h>
void memoire_remplissage() {
    int* jours = calloc(3, sizeof(int));
    memset(jours, 1, 3*sizeof(int)); // initialise avec des 1
    printf("Jours ville 0 : %d\n", jours[0]); // 16843009 = 2^24 + 2^16 + 2^8 + 1
    free(jours);

    int* jours2 = calloc(4, sizeof(int));
    jours2[0] = 3;
    jours2[1] = 5;
    memcpy(jours2 + 2, jours2, 2*sizeof(int)); // dest, src, longueur
    printf("Jours de la 3eme ville : %d\n", jours2[2]); // 3
    // NON : memcpy(jours2 + 1, jours2, 2*sizeof(int));
    memmove(jours2 + 1, jours2, 2*sizeof(int));
    //C11
    // OU memmove_s(jours2 + 1, (4-1)*sizeof(int), jours2, 2*sizeof(int) );
    // NON : printf("%d", jours2[0]);
    // Au final : 3? 3 5 5
    free(jours2);

	int* jours3 = calloc(1, sizeof(int));
	jours3[0] = 1;
	for(size_t i = 0; i < sizeof(int) ; i++)
        printf("%d, ", ((char*)jours3)[i]); // 1, 0, 0, 0, : petit-boutiste
	free(jours3);
}

// #include <ctype.h>
void chaines_de_caracteres() {
    // OU char* ville1 = "Mexico";
    // OU char ville1[] = {'M', 'e', 'x', 'i', 'c', 'o', 0};
    unsigned char ville1[] = {77, 101, 'x', 'i', 'c', 'o', 0};
    printf("\nBienvenido a %s\n", ville1);
    unsigned char trajet[100]; // OU [16]
    // strcpy(trajet, ville1);
    // strcat(trajet, " - ");
    // strcat(trajet, "Cancun");
    strcpy_s(trajet, 100, ville1);
    strcat_s(trajet, 100, " - ");
    strcat_s(trajet, 100, "Cancun");
    printf("Trajet : %s (%I64d)\n", trajet, strlen(trajet)); // ... (15)
    if(strcmp(trajet, "Cancun - Mexico") == 0) // faux
        printf("Identique\n");
    if(strstr(trajet, "nc")!=NULL) {
        unsigned n = (unsigned char *)strstr(trajet, "nc") - trajet;
        printf("nc est a la position %d du trajet\n", n); // 11
    }
    if(islower(trajet[0]))
        trajet[0] = toupper(trajet[0]);
}

void en_cas_dinterruption(int signal) {
    printf("Signal %d\n", signal); // 2
    // exit(0), quick_exit(0), abort()
}

void en_cas_de_sortie() {
    printf("Au revoir\n");
}

// #include <signal.h>
void signaux() {
    void (* ancien_receveur)(int);
    ancien_receveur = signal(SIGINT, en_cas_dinterruption);
    atexit(en_cas_de_sortie);

    raise(SIGINT);
    signal(SIGINT, ancien_receveur);
    // NON :
    // raise(SIGINT);
    // signal(SIGINT, en_cas_dinterruption);
}

// #include <stdint.h>
// #include <math.h>
// #include <complex.h>
void nombres() {
    // short voyageurs_annuels = 2349;
    // int voyageurs_annuels = 2349; // >= 2o
    // long voyageurs_annuels = 2349; // >= 4o
    // long long voyageurs_annuels = 2349; // >= 8o
    uint32_t voyageurs_annuels = 2349; // 4o, peut �chouer, C99
    int_fast8_t jours_par_mois = 31; // >=1o, le + rapide possible, C99
    int_least16_t jours_par_ans = 366; // >=2o, le + petit possible, C99
    if(jours_par_ans <= UINT_LEAST16_MAX) // 65535, UINT_LEAST16_MIN=0
        printf("Tout est nominal\n");
    intmax_t secondes_de_voyages;
    printf("Taille max : %d\n", sizeof(secondes_de_voyages)); //8
    intptr_t secondes_de_voyages_ptr = (intptr_t) &secondes_de_voyages;
    // printf("Mois par ans : %d\n", jours_par_ans/jours_par_mois); // 11
    printf("Mois par ans : %I32d\n", lround((double)jours_par_ans/jours_par_mois) );
    double complex position = 43.5 + 1.2I; // standard
    //_Dcomplex position = {43.5, 1.2}; // Visual Studio
    printf("Distance a 0,0 : %f\n", cabs(position)); // 43.516549
}

// #include <stdbool.h>
void booleens() {
    bool promo_activees = false;
    if(promo_activees)
        printf("Promos activ�es\n");
}

// #include <time.h>
void dates_et_heures() {
    time_t maintenant = time(NULL);
    struct tm tm_depart;
    tm_depart.tm_year = 2023-1900;
    tm_depart.tm_mon = 11; // decembre
    tm_depart.tm_mday = 17;
    tm_depart.tm_hour = tm_depart.tm_min = tm_depart.tm_sec = 0;
    time_t depart = mktime(&tm_depart);
    double a_attendre = difftime(depart, maintenant); // en secondes
    printf("Depart dans %d jours et %d heures\n",
           (int)a_attendre / (24*3600),
           ((int)a_attendre / 3600) % 24); // 11 jours et 8 heures
    //struct tm tm_maintenant = *localtime(&maintenant);
    struct tm tm_maintenant;
    localtime_s(&maintenant, &tm_maintenant); // GCC
    // localtime_s(&tm_maintenant, &maintenant); // MsVS
    printf("Il est %02d:%02d\n", tm_maintenant.tm_hour, tm_maintenant.tm_min);
}

// #include <stdarg.h>
void afficher_detail(const char* pays, ...) {
    unsigned total = 0;
    va_list liste;
    va_start(liste, pays); // la liste commence juste apr�s "pays"
    unsigned v = -1;
    while(v != 0) {
        v = va_arg(liste, unsigned);
        total += v;
    }
    va_end(liste);
    printf("%s : %d euros\n", pays, total);
}

void fonctions_variadiques() {
    afficher_detail("Perou", 890, 367, 198, 54, 0);
    afficher_detail("Venezuela", 921, 62, 0);
}

void bibliotheques() {
    printf("* Bibliotheques :\n");
    memoire();
    communication_memoire();
    memoire_allocations();
    memoire_remplissage();
    chaines_de_caracteres();
    signaux();
    nombres();
    booleens();
    dates_et_heures();
    fonctions_variadiques();
}
