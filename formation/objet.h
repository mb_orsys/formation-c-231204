#ifndef OBJET_H_INCLUDED
#define OBJET_H_INCLUDED

void objet();

struct Destination {
    const char * nom;
    unsigned short jours;
    void (* afficher)(struct Destination* d);
    void (* allonger)(struct Destination* d, short j);
    void (* raccourcir)(struct Destination* d, short j);
};
typedef struct Destination Destination;

Destination* destination_ctor(const char * nom, unsigned short jours);
void destination_dtor(Destination* self);

#endif // OBJET_H_INCLUDED
