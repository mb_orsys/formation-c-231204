#include <stdio.h>

#include "pointeurs.h"
#include "bibliotheques.h"
#include "recursivite.h"
#include "objet.h"
#include "nouveautes.h"

// execution :
// formation.exe OU
// formation.exe -paspointeurs
int main(int argc, char *argv[], char* envp[])
{
    printf("Bienvenue dans l'agence!\n");
    if(argc>=2 && argv[1][0]=='-' && argv[1][1]=='p') // etc.
        printf("pointeurs() non executee\n");
    else
        pointeurs();
    if(envp[0]!=NULL) // NULL = tableau de 0 chaines
        printf("1ere variable d'environnement : %s\n", envp[0]);

    bibliotheques();
    recursivite();
    objet();
    nouveautes();
    return 0;
}
