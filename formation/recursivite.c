#include <stdio.h>

unsigned mul5(unsigned x) {
    if (x<=0)
        return 0;
    return 5+mul5(x-1);
}

unsigned fibo(unsigned n) {
    if (n < 2)
        return 1;
    return fibo(n-1)+fibo(n-2);
}

// arbre (et non : graphe orient�, graphe)
struct Etape {
    const char* nom;
    unsigned jours;
    struct Etape* etapes[5];
};
typedef struct Etape Phase;

void afficher_etape(Phase p) {
    printf("%s : %dj\n", p.nom, p.jours);
}
void afficher_etape_ptr(Phase* p) {
    printf("%s : %dj\n", (*p).nom, p->jours);
}

unsigned nb_etapes(Phase p) {
    unsigned resultat = 1;
    for(size_t i = 0; p.etapes[i]!=NULL ; i++)
        resultat += nb_etapes(* p.etapes[i] );
    return resultat;
}

unsigned nb_jours(Phase* p) {
    unsigned resultat = p->jours;
    for(size_t i = 0; p->etapes[i]!=NULL ; i++)
        resultat += nb_jours(p->etapes[i]);
    return resultat;
}

void recursivite() {
    printf("* Recursivite\n");
    printf("mul5(3) : %d\n", mul5(3)); // 15
    unsigned ville_bresiliennes = 41;
    printf("Trajets au Bresil : %d\n", fibo(ville_bresiliennes)); // 267914296
    // struct Etape e1;
    Phase e1;
    e1.nom = "Bresil";
    e1.jours = 3;
    // afficher_etape(e1);
    afficher_etape_ptr(&e1);
    Phase e2 = { "Sao Paolo", 2, {NULL, NULL, NULL, NULL, NULL } };
    Phase e3 = { .jours=3, .nom="Amazone", .etapes = {NULL, NULL, NULL, NULL, NULL } };
    Phase e4 = { "Maranon", 1, {NULL, NULL, NULL, NULL, NULL } };
    Phase e5 = { "Rio Huallaga", 4, {NULL, NULL, NULL, NULL, NULL } };
    e1.etapes[0] = &e2;
    e1.etapes[1] = &e3;
    e1.etapes[2] = NULL;
    e3.etapes[0] = &e4;
    e3.etapes[1] = &e5;
    // graphe orient� - ok :
    e2.etapes[0] = &e4;
    // graphe
    // e5.etapes[0] = &e1;
    printf("Nombres d'etapes pour Rio Huallaga : %d\n", nb_etapes(e5)); // 1
    printf("Nombres d'etapes pour Bresil : %d\n", nb_etapes(e1)); // 6
    printf("Temps total au Bresil : %d\n", nb_jours(&e1)); // 14
}
