#include <stdio.h>

#include "pointeurs.h"

void creation_de_pointeurs() {
    float prix_alaska = 967.99f; // 4 octets
    // float * pa_ptr, * f2;
    float * pa_ptr = &prix_alaska; // 8 octets
    printf("Nos voyages en Alaska : %.02f euros\n", *pa_ptr );
    // printf("Nos voyages en Alaska : %.02f euros\n", *&prix_alaska ); // 967.99
    // NON - � gauche d'un �gal, que des "lvalue" : int b; -b = 18;
    *pa_ptr += 100;
    printf("Nos voyages en Alaska (2024) : %.02f euros\n", *pa_ptr ); // 1067.99
    printf("Nos voyages en Alaska (2024) : %.02f euros\n", prix_alaska ); // 1067.99
    // pa_ptr = 1000000;
    // printf("Nos voyages en Alaska (2024) : %.02f euros\n", *pa_ptr ); // segfault
    pa_ptr = NULL;
    // C23 : pa_ptr = nullptr;
    // printf("Nos voyages en Alaska (2024) : %.02f euros\n", *pa_ptr ); // segfault
    pa_ptr = &prix_alaska;
    printf("Adresse 1 : %d\n", pa_ptr ); // 6421940
    pa_ptr = pa_ptr + 1; // float : + 4 octets
    printf("Adresse 2 : %d\n", pa_ptr ); // 6421944
    printf("Adresse 2 : %p\n", pa_ptr ); // 000000000061FDB8
    // NON : *pa_ptr += 10;
    unsigned long long vptr = pa_ptr; // PC64
    void* vptr2 = pa_ptr;
    // vptr2 = vptr2 + 1; // 1 octet
    // *vptr2 = 1189.99f;

    // v est une variable de n'importe quel type, au choix
    // **v // OUI - v est un pointeur sur un pointeur, par exemple : float** v;
    // float ** v = &pa_ptr;
    // printf("%.2f", **v);

    // &&v // NON, de m�me que &(3+4) ou &(prix_alaska*2)
    // *&v // OUI, inutile
    // &*v // OUI, inutile
}

// unsigned lire_moins_cher(double prix[7]) {
// OU unsigned lire_moins_cher(double prix[]) {
// OU
unsigned lire_moins_cher(double* prix, size_t taille) {
    unsigned jour_moins_cher = 0;
    for(size_t i = 1 ; i < taille ; i ++)
        if(prix[i] < prix[jour_moins_cher])
            jour_moins_cher = i;
    return jour_moins_cher;
}

void tableaux() {
    // dimanche : 0
    double prix[7] = { 972.99, 980, 927, 834, 934.99, 890.99, 1090.99 };
    printf("Prix le mardi : %.2f euros\n", prix[2]); // 927.00
    prix[4] *= 0.9; // promo de -10% le jeudi
    // double * prix_ptr = & prix[0]; // EQUIVAUT A :
    double * prix_ptr = prix;
    printf("Prix le jeudi : %.2f euros\n", *(prix_ptr+4) ); // 841.49

    printf("Taille du tableau : %d\n", sizeof(prix)); // 56 octets
    unsigned jour_moins_cher = lire_moins_cher(prix, sizeof(prix)/sizeof(double));
    printf("Jour moins cher : %d\n", jour_moins_cher); // 3 = mercredi
}

void afficher_debut(double departs[4][2]) {
    printf("Premier element : %f\n", departs[0][0]); // 43.6
}

void tableaux_multi_dimensionnels() {
    // Mickael, Tom, Baudoin, Mohmed
    double departs[][2] = { { 43.6, 1.4 }, { 45.6, 3.0 }, { 48.8, 2.2 }, { 48.9, 2.2 } };
    // EQUIVAUT A : double departs[] = { 43.6, 1.4, 45.6, 3.0, 48.8, 2.2,  48.9, 2.2  };
    printf("Depart de chez Tom : %f,%f \n", departs[1][0], departs[1][1]);
    double (* departs_ptr)[2] = departs; // pointe sur le 1er sous-tableau
    printf("Latitude de Baudoin : %f  \n", ( *(departs_ptr+2) ) [0]); // +2 = +32 octets
    double * departs_ptr2 = (double*)departs; // OU Warning conversion
    printf("Longitude Mohmed : %f \n", *(departs_ptr2+3*2+1)); // 2.2
    afficher_debut(departs);
}

void vlas() { // C99+
    int nb_personnes = 2;
    short nb_infos = 2; // 3 avec altitude
    double departs_dyn[nb_personnes][nb_infos]; // echec avec VS
    departs_dyn[0][0] = 43.6;
    departs_dyn[0][1] = 1.4;
    departs_dyn[1][0] = 45.6;
    departs_dyn[1][1] = 3.0;
    printf("Fin du VLA : %f\n", departs_dyn[1][1]);
}

unsigned get_prix_canada(const unsigned jours) {
    return 860 + jours * 48;
}

unsigned get_prix_canada_promo(const unsigned jours) {
    const unsigned promo = 20; // -20% !
    return (100-promo) * get_prix_canada(jours) / 100;
}

void afficher_prix_canada(unsigned (* fonction_calcul)(const unsigned), unsigned jours[]) {
    size_t i = 0;
    while(jours[i] != 0) {
        printf("Pour %dj : %d\n", jours[i], fonction_calcul(jours[i]));
        i++;
    }
}

#define PROMO_ACTIVEE 1
void pointeurs_de_fonctions() {
    unsigned (* fonction_calcul)(const unsigned);
    // NON, aucun sens : unsigned (const* fonction_calcul)(const unsigned);
    // NON, modifi� ci-dessous : unsigned (* const fonction_calcul)(const unsigned);
    if(PROMO_ACTIVEE)
        fonction_calcul = get_prix_canada_promo;
    else
        fonction_calcul = get_prix_canada;
    unsigned jours[] = { 6, 8, 9, 0 };
    unsigned (* fonctions_calcul[])(const unsigned) = { fonction_calcul };
    afficher_prix_canada(fonctions_calcul[0], jours);
}

void constantes() {
    double prix_mexique = 490.80;
    const double inflation = 1.08;
    // NON : inflation += 0.02
    double * const prix_mexique_ptr = &prix_mexique; // pointeur constant
    const double * inflation_ptr = &inflation; // pointeur sur constante
    // NON : *inflation_ptr += 0.02;
    prix_mexique *= inflation;
    // NON : prix_mexique_ptr++;
    // NON : prix_mexique_ptr = &prix_mexique;
    printf("Prix Mexique : %.2f\n", prix_mexique);
}

void pointeurs() {
    printf("* Pointeurs\n");
    creation_de_pointeurs();
    tableaux();
    tableaux_multi_dimensionnels();
    vlas();
    pointeurs_de_fonctions();
    constantes();
}
