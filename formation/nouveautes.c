#include <stdio.h>
#include <string.h>
#define NDEBUG
#include <assert.h>

void unions() {
    union UnionXouY {
        int x;  float y;
    };
    union UnionXouY u1;
    u1.x = 1000000000;
    printf("u1.y : %f ; taille : %do\n", u1.y, sizeof(u1)); // 0.004724  4o

    struct SUnionXouY {
        char xouy; // 'x' ou 'y'
        union {
            int x;  float y;
        };
    };
    struct SUnionXouY s1;
    s1.y = 23.9f;
    s1.xouy = 'y';
    // ...
    if(s1.xouy=='x')
        printf("s1 : %d\n", s1.x);
    else
        printf("s1 : %f\n", s1.y);
    printf("Taille s1 : %do\n", sizeof(s1)); // 8o : |c...iiii| ou |c...ffff|
    // printf("Alignement de s1 : %d\n", _Alignof(s1)); //4 - GCC/MinGW
    printf("Alignement de s1 : %d\n", __alignof(s1)); //4 - Tout
}

#define NOM_TYPE(var) _Generic(var, short:"sh", int:"in", default:"?")

void generiques() {
    int i = 3;
    double d = 5.0;
    printf("Types : %s et %s \n", NOM_TYPE(i+i), NOM_TYPE(d)); // in et ?
}

void utf8() {
    const char* chaine = u8"Chaine : 😊";
    printf("Exemple : %s\n", chaine); // Chaine : ­ƒÿè
    printf("Longueur : %d\n", strlen(chaine)); // 13 et non 10
}

#define PRC_MAX_PROMO 80

void assertions_statiques() {
    int promo = 18;
    _Static_assert( PRC_MAX_PROMO<=100, "PRC_MAX_PROMO doit etre entre 1 et 100" );
    assert(promo < PRC_MAX_PROMO);

    printf("Promo : %d\n", promo);
}

void nouveautes() {
    printf("* Nouveautes\n");
    unions();
    generiques();
    utf8();
    assertions_statiques();
}
